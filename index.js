
const FIRST_NAME = "Cristian-Alin";
const LAST_NAME = "Puican";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching() {
   let page = {};

   page.pageAccessCounter = function(section){

       if(section===undefined){

            if(page.hasOwnProperty("home")){
                page["home"]++;
            }
             else{
                page["home"]=1;
            }
       }
       else{
           let sect = section.toLowerCase();
            if(page.hasOwnProperty(sect)){
                page[sect]++;
            }
            else{
                page[sect]=1;
            }
       }
       
   };


   page.getCache = function(){
       return this;
   };


   return page;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

